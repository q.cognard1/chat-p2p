## Chat Peer to peer

Chat p2p permettant la communication , sans serveur, de plusieurs clients dans un même réseau local.
Le chat marche en TCP et possède plusieurs commandes :
+  BM ( pour parler à tous le monde )
+  PM [pseudo] ( pour envoyer un message privé )
+  BAN [pseudo]
+  UNBAN [pseudo]
+  Man CMD