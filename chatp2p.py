﻿#!/usr/bin/python

from sys import *
from select import *
from socket import *
import fcntl
import struct
import time

# fonction prennant un message en paramètre et envoyant un message a toutes les sockets de la liste socks
def envoieBM(msg):
    for c in socks:
        msge=''
        if c not in [stdin,s]:
            msge='57791BM#'+msg+"\r\n"
            msge=msge.encode('utf_8','strict')
            c.send(msge)
#fonction prennant un pseudo et un message et envoyant un message uniquement à la socket concerné
def envoiePM(nick,msg):
    if nick==nickname:
        print("vous ne pouvez pas vous MP vous-même .......")
        return False
    else:
        for c in socks:
            msge=''
            if c not in [stdin,s] and diconic.get(c)==nick:
                msge='47791PM#'+msg+"\r\n"
                msge=msge.encode('utf_8','strict')
                c.send(msge)
                return True

#fonction pour quitter le chat ( qui prévient toutes les sockets que l'on part )
def quit_chat():
    for c in socks:
        if c not in [stdin,s]:
            msg='quit'+monIP
            msg=msg.encode('utf-8')
            c.send(msg)
    s.shutdown(SHUT_RDWR)
    s.close()


#fonction pour bannir quelqu'un pour ne plus voir ses messages
def bannir(pseudo):
    bannis.append(pseudo)
    print("liste des bannis:")
    print(bannis)

#fonction pour débannir quelqu'un
def unbannir(pseudo):
    try:
        bannis.remove(pseudo)
        print("liste des bannis:")
        print(bannis)
    except:
        print(pseudo+" n'est pas un pseudo bannis")
#fonction qui prend l'interface ( nous ca va être eth0 ) et qui nous renvoie notre adresse IP, le gethostname ne marchant pas (cela renvoie 127.0.0.1 )
def get_ip_address(ifname):
    s = socket(AF_INET,SOCK_DGRAM)
    return inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

print(get_ip_address('eth0'))

monIP=get_ip_address('eth0')
print('Pseudo ?')
nickname=stdin.readline().strip("\n").strip()

# liste de socket
socks=[]
#liste des ip
laddr=[monIP]
#dictionaire socket->pseudo
diconic={}
#liste des bannis
bannis=[]

# si on à un argument
if len(argv)>1:
    sockcli=socket(AF_INET,SOCK_STREAM)
    sockcli.connect((argv[1],1664))
    laddr.append(argv[1])
    socks.append(sockcli)
    sockcli.send("17791START#"+nickname+'\r\n')



s=socket(AF_INET,SOCK_STREAM)
s.bind(("0.0.0.0",1664))
s.listen(1000)
socks.append(s)
socks.append(stdin)
print("Bienvenue sur le chat P2P ! ")
while True:
    try:
        connections_demandees, wlist, xlist =select(socks,[],[])
    except:
        print("connexion terminé !  ")
    for t in connections_demandees:
        if t == stdin :
            # à partir d'ici, on à détecter une entrée clavier et on execute donc la commande demandée
            data = stdin.readline().strip("\n")
            if data.split(' ')[0]=="quit" or data.split(' ')[0]=="QUIT":
                print(nickname+" quitte")
                quit_chat()
                break
            try:
                chaine=data.split(' ')
                cmd=chaine[0]
                if cmd=='BM' or cmd=='bm':
                    envoieBM(data.split(' ', 1)[1])
                    print ("[BM] "+nickname+" à dit : "+data)
                elif cmd=='PM' or cmd=='pm':
                    try:
                        rep=envoiePM(data.split(' ', )[1],data.split(' ', 2)[2])
                        if rep:
                            print ("[PM] "+nickname+" à dit -> "+data.split(' ', 2)[2])
                        else:
                            print("Message non envoyé")
                    except:
                        print(cmd+" n'est pas une commande valide ou mal utilisé, voir : 'man cmd', le pseudo n'existe peut être pas ?")
                elif cmd=="ban" or cmd=="BAN":
                    bannir(data.split(' ', 1)[1])
                elif cmd=="unban" or cmd=="UNBAN":
                    unbannir(data.split(' ', 1)[1])
                elif cmd=="man" or cmd=="MAN":
                    print("1. BM [message]")
                    print("2. PM [nickname] [message]")
                    print("3. ban [nickname]")
                    print("4. unban [nickname] ")
                    print("5. quit or QUIT")
                else:
                    print(cmd+" n'est pas une commande valide ou mal utilisé, voir : 'man cmd'")
            except:
                print(" vous devez taper une commande : obtenez la liste des commandes en tapant 'man cmd'")

        elif t == s:
            #on accepte les nouvelles socket
            (c,addr)=s.accept()
            if addr[0] not in laddr:
                laddr.append(addr[0])
                socks.append(c)

        elif t not in [stdin,s] :
            # ici on recoit les données des utilisateurs connectés
                data=t.recv(1024)

                # si on reçoit un start
                if data[0:4]=='1779':
                    data=data.decode('utf_8','strict')
                    chaine = data[5:]
                    chaine = chaine[:-2]
                    print("Message start reçu")
                    cmd,nic=data.split('\043')
                    if t not in diconic.keys():
                        nic=nic.strip("\n").strip("\r")
                        diconic[t]=nic
                    t.send('27791HELLO#'+nickname+'\r\n')
                    chainelistaddr=''
                    cpt=0
                    for ad in laddr:
                        cpt+=1
                        if cpt==len(laddr):
                            chainelistaddr+=ad
                        else:
                            chainelistaddr+=ad+','
                    time.sleep(1)
                    t.send('37791IPS#'+chainelistaddr+'\r\n')

                # si on reçoit un hello
                elif data[0:4]=='2779':
                    data=data.decode('utf_8','strict')
                    chaine = data[5:]
                    chaine = chaine[:-2]
                    print("Message hello reçu")
                    nic=chaine[6:]
                    if t not in diconic.keys():
                        nic.strip("\n").strip("\r")
                        diconic[t]=nic
                        t.send('27791HELLO#'+nickname+'\r\n')

                # si on reçoit un IPS avec la liste des ips
                elif data[0:4]=='3779':
                    data=data.decode('utf_8','strict')
                    chaine = data[5:]
                    chaine = chaine[:-2]
                    print("Message IPS reçu")
                    cmd,list=chaine.split('\043')
                    ipList=list.split(',')
                    for ip in ipList:
                        if ip not in laddr:
                            laddr.append(ip)
                            newsock=socket(AF_INET,SOCK_STREAM)
                            newsock.connect((ip,1664))
                            socks.append(newsock)
                            newsock.send('27791HELLO#'+nickname+'\r\n')

                #si on reçoit un PM
                elif data[0:4]=='4779':
                    data=data.decode('utf_8','strict')
                    chaine = data[8:]
                    chaine=chaine.strip("\n").strip("\r")
                    pseudoC=diconic.get(t)
                    if pseudoC not in bannis:
                        print("[PM] "+pseudoC+" a dit ->"+chaine)

                # si on reçoit un BM
                elif data[0:4]=='5779':
                    data=data.decode('utf_8','strict')
                    chaine = data[8:]
                    chaine=chaine.strip("\n").strip("\r")
                    #nic_sender=diconic[t]
                    try:
                        pseudoC=diconic.get(t)
                        if pseudoC not in bannis:
                            print("[BM] "+pseudoC+" a dit :"+chaine)
                    except:
                        print("[BM] inconnu a dit :"+chaine)

                # si on reçoit un quit
                elif data[0:4]=='quit':
                    ipquit=data[4:]
                    if ipquit in laddr:
                        print(diconic.get(t)+" a quitter le session chat")
                        laddr.remove(ipquit)
                        socks.remove(t)
                        t.shutdown(SHUT_RDWR)
                        t.close()
